<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET,POST');

//SENT PARAMETERS FROM JS
$device_id = $_POST['device_id'];

//OPEN JSON FILE
$json = file_get_contents('./users.json');
$obj = json_decode($json, true);
$arrayName = array();


//DECODE TO ARRAY
foreach ($obj as $item) {
	$item_device = $item['device_id'];
	//IF REGISTERED
	if($device_id == $item_device){
		$arrayName = array('status' => 1);
		break;
	}else{
		$arrayName = array('status' => 0 );
	}
}

echo json_encode($arrayName);
//return 1 if registered 0 if not
?>