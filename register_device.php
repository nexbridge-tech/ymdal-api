<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET,POST');

//SENT PARAMETERS FROM JS
$drno = $_POST['drno'];
$outlet = $_POST['outlet_code'];
$device_id = $_POST['device_id'];

//OPEN JSON FILE
$json = file_get_contents('./users.json');

//DECODE TO ARRAY
$obj = json_decode($json, true);

//INITIALIZE NEW ARRAY TO ADD IN JSON FILE
$new_data = array();
$new_data["drno"] = $drno;
$new_data["outlet"]=$outlet;
$new_data["device_id"] = $device_id;
$new_data["status"] = "PENDING"; // new registered = pending

//PUSH TO ADD THE NEW ARRAY
array_push($obj, $new_data);


//ENCODE ARRAY TO JSON
$new_file = json_encode($obj);

//SAVE/REPLACE FILE
file_put_contents('./users.json', $new_file);

//save drno, outlet code and device id returns 0 for failure 1 if succeed.
?>