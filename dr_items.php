<?php 
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET,POST');

$json = file_get_contents('./dr_items.json');
$obj = json_decode($json, true);

$new_json = json_encode($obj);

echo $new_json;
?>