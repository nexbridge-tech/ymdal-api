<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET,POST');

//SENT PARAMETERS FROM JS
$device_id = $_POST['device_id'];

//OPEN JSON FILE
$json = file_get_contents('./users.json');

//DECODE TO ARRAY
$obj = json_decode($json, true);

//FOR EACH INDEX AS ITEM GET PROPERTIES
foreach ($obj as $item) {
	$item_drno = $item['drno'];
	$item_outlet = $item['outlet'];
	$item_device = $item['device_id'];
	if($device_id == $item_device){ 
		$item_status = $item['status'];
		if($item_status == 'PENDING')
			echo json_encode($arrayName = array('status' => 0));
		else
			echo json_encode($arrayName = array('status' => 1));
		break;
	}
}
//return registration status = Pending or Approved
?>