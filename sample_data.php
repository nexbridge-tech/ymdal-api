<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET,POST');

class Objects{
    function Objects() {
        $this->dr_no = "DJ000565";
        $this->atp_no = "181651";
        $this->dr_date = "2018-07-17";
        $this->po_no = "18-06-003";
        $this->outlet = "YAMAHA WHEELTEK Sta. Ignacia";
        $this->tel = "+639437093745";
        $this->address = "C20";
        $this->email = "";
        $this->sdr_no = "18G06807";
        $this->carrier = "YUSEN";
    }
}
$obj = new Objects();
$json_ = json_encode($obj);

echo $json_;
?>